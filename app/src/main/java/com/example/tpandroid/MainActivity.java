package com.example.tpandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.BreakIterator;


public class MainActivity extends AppCompatActivity {
    String login;
    String password;
    TextView Result;
    JSONObject result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Result = findViewById(R.id.result);
    }
    Runnable runnable = new runnable_thread();
    Thread thread = new Thread(runnable);


    public void onClick(View view) {
        EditText login_id = findViewById(R.id.editTextTextPersonName);
        EditText password_id = findViewById(R.id.editTextTextPassword);
        login = login_id.getText().toString();
        password = password_id.getText().toString();
        thread.start();
    }
    private String readStream(InputStream is) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int i = is.read();
            while (i != -1) {
                bo.write(i);
                i = is.read();
            }
            return bo.toString();
        } catch (IOException e) {
            return "";
        }
    }

    private class runnable_thread implements Runnable {
        @Override
        public void run() {
            System.out.println("MyThread running");
            URL url = null;
            try {
                url = new URL("https://httpbin.org/basic-auth/" + login + "/" + password);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                String basicAuth = "Basic " + Base64.encodeToString((login + ":" + password).getBytes(), Base64.NO_WRAP);
                urlConnection.setRequestProperty("Authorization", basicAuth);
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String s = readStream(in);
                    Log.i("JFL", s);
                    MainActivity.this.result = new JSONObject(s);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    urlConnection.disconnect();
                }
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            try
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = MainActivity.this.result;
                            Result.setText("My result here " + json.get("authenticated"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

